There is a [pearl on YouTube](https://www.youtube.com/watch?v=fQV1P29qAyg) ([bonus](https://m.youtube.com/watch?v=n9Fus93I-HY)), lets be sure to properly save it before it gets deleted!


First of all, create the directory and get inside of it
```
mkdir liquid_democracy && cd liquid_democracy
```

Then run a Python docker container and install the required softwares
```
docker run -it --rm -v "${PWD}:/app" -p 8080:8000 python bash
pip install  waybackpack youtube-comment-downloader youtube-dl
cd /app
```

Download the videos!
```
youtube-dl fQV1P29qAyg
youtube-dl n9Fus93I-HY
```

Download the comments!
```
youtube-comment-downloader --youtubeid fQV1P29qAyg --output fQV1P29qAyg_comments.json
youtube-comment-downloader --youtubeid n9Fus93I-HY --output n9Fus93I-HY_comments.json
```

For the bonus video, there's a [wayback machine snapshot](https://web.archive.org/web/20160223042130/http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/) of a blog entry...
```
waybackpack http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ -d waybackmachine_zuse-crew
```

<details>
    <summary>Result</summary>
INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130607195345
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130607195345/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130607195345
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130607195345/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130610012042
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130610012042/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130711044903
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130711044903/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130711044903
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130711044903/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20131203004154
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20131203004154/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20160223042130
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20160223042130/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20160703045719
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20160703045719/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20170928224031
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20170928224031/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20211002040535
INFO:waybackpack.session: HTTP status code: 301
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20211002040535/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

root@931fd79440b2:/app/aaa# cd ..
root@931fd79440b2:/app# waybackpack http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ -d waybackmachine_zuse-crew
INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130607195345
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130607195345/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130607195345
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130607195345/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130610012042
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130610012042/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130711044903
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130711044903/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20130711044903
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20130711044903/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20131203004154
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20131203004154/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20160223042130
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20160223042130/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20160703045719
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20160703045719/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20170928224031
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20170928224031/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html

INFO:waybackpack.pack: Fetching http://zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/ @ 20211002040535
INFO:waybackpack.session: HTTP status code: 301
INFO:waybackpack.pack: Writing to waybackmachine_zuse-crew/20211002040535/zuse-crew.de/blog/2013/05/17/blickwinkel-hausverbot-christian-jacken/index.html
</details>

 

Also there is a [PDF file](https://wiki.piratenpartei.de/Datei:Liquid_Democracy_SMV_SMVcon_Christian_Jacken.pdf).
```
wget https://wiki.piratenpartei.de/Datei:Liquid_Democracy_SMV_SMVcon_Christian_Jacken.pdf
```

This is it.


**PS:** If the 'youtube-comment-downloader' gets lost, theres a [fork of it](https://github.com/osouza-de/youtube-comment-downloader).
